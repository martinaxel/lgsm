# How does LGSM work ?

This document will show you the general LGSM's architecture.


## Global's architecture


LGSM requires 9 files & folders. Most of them are config files which means you 
have to respect a determined syntax in order to adapt it as you please.

It should be like that :

    LGSM                    # Root folder
    |   base.cfg            # Default config file, do not touch
    |   buildout.cfg        # Buildout & eggs config file, import base.cfg
    |   bootstrap.py        # Python script which installs buildout
    |   bin/                # Contains all the engines formated by LGEC
    |   config/             # Game's configs by user
    |   doc/                # Software documentation
    |   develop-eggs/       # Contains all the developed engines
    |   eggs/               # Official eggs, do not develop here
    |   vars/               # Export of all configs 


## Buildout installation


First of all, you have to retrieve the `bootstrap.py` file. Then execute it by 
python by : `python2 bootstrap.py`.

It will create the `bin/buildout` file, which is the environment creator. By 
executing it with `bin/buildout`, you will be able to install BuildOut. Then 
all eggs should be placed and fully fonctionnal.

It imports the `buildout.cfg` file, which you may be able to configure, in 
order to execute extra command lines.

It generates the LGSM's developed eggs and makes the application ready to run.


