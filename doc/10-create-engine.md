# How to create your own server engine ?

This document will guide you while you're creating a compatible server engine
with LGSM.


## Step 1: create an egg

First of all you'll need to create an egg. Install it in the `develop-egg`
folder with the following name : `Engine.ACoolName`. The architecture of the
folder should be like that :

    Engine.ACoolName                    # Root folder
    |   README.txt                      # Containing informations about the egg
    |   setup.py                        # (1)
    |   src/                            # Python source code
    |   |   Engine/                     # First level module. Do not place anything else here
    |   |   |   __init__.py             # Only contains `__import__('pkg_resources').declare_namespace(__name__)`
    |   |   |   ACoolName/              # The module engine. Place the following files and other if you want to
    |   |   |   |   __init__.py         # (2)
    |   |   |   |   configure.lgec      # (3)
    |   |   |   |   engine.py           # (4)
    |   |   |   |   deploy/             # The module binarys and files
    |   |   |   |   |   [your server files]


### `setup.py` : buildout setup of your egg

This file will setup the egg like you want. You can modify and append things if
you want, but a basic setup would be like this :

    #!/usr/bin/python
    from setuptools import setup, find_packages

    version = "1.0"                         # modify this as you want
    author  = "Foo Dupont"                  # modify this as you want
    email   = "foo@dupont.com"              # modify this as you want
    name    = "Engine.ACoolName"            # set the Python package name here
    require = ['zc.buildout', 'LGSM.lgec']  # if you want to add other modules, place them here

    setup(
        name                    = name,
        version                 = version,
        author                  = author,
        author_email            = email,
        package_dir             = {'' : 'src'},
        packages                = find_packages('src'),
        namespace_packages      = ['Engine'],
        install_requires        = require,
        include_package_data    = True,
        zip_safe                = False,
        entry_points            = {
            'LGSM': ['default = %s:initialize' % name]
            }
        )

Basicly the entry point for LGSM engine configuration will be in the file
`Engine.ACoolName/src/Engine/ACoolName/__init__.py` as a function named `initialize`.
If you want to modify this entry point, you can.


### `__init__.py` : definition of initialize function for LGSM

A standard `__init__.py` file should contains that stuff :

    #!/usr/bin/python
    import os
    
    from LGSM.lgec import LGECReader

    def initialize(context):
        this_path = os.path.dirname(os.path.realpath(__file__))
        LGECReader(from_file = this_path + "/configure.lgec").register(context)

This function allows LGSM to register the LGEC (LGSM Game Engine Configuration)
file into his database. This is the main utility of this.

If you want to do some specials things at each buildout, you can also define it
into this function. It's useful if for exemple, you have to clear all logs at
each buildout.


### `configure.lgec` : configuration of your engine

This file is here to tell LGSM some stuff about your executable. It's an XML
file with a DTD file located in the LGSM.lgec egg. This file has to respect
this DTD (we are aware it's anoying, but you have to !).

Here is a standard XML configure file :

    <Engine package="Engine.ACoolName" engine="Engine.ACoolName.engine:ACoolNameEngine">
    
        <!-- MetaDatas configurations -->
        <MetaData>
            <FullName>A cool name</FullName>    <!-- the full name of your server -->
            <EngineKey>acn</EngineKey>          <!-- short name for command line -->
            <EngineVersion>1.0</EngineVersion>  <!-- facultative: version of the engine -->
            <EggVersion>1.0</EggVersion>        <!-- version of the egg -->
            <Category>Game</Category>           <!-- utility, game... -->
        </MetaData>

        <!-- A config key. You can add multiple config keys -->
        <ConfigKey
            key="xxx"
            default="yyy"
            validator="none"
            />

        <!-- Folder configuration -->
        <FolderConfig base="deploy">
            <Binary path="the binary path in deploy" os="all" arch="64bits" />
            <Binary path="the 32 binary path in deploy" arch="32bits" />    <!-- if os isn't specified, default is all -->
        </FolderConfig>
    </Engine>


### `engine.py` : the core engine

TODO
