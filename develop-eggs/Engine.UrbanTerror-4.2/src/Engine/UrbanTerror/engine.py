## 
# @file engine.py
# @author Axel Martin <axel.martin@eisti.fr>
# @brief LGSM Urban Terror Game Engine
# @version 0.1
# @date 2015-02-09
# 

## 
# Copyright (C) 2015 Axel Martin 
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public Licence
# as published by the Free Software Foundation; eitther version 2
# of the Licence, or (at your option) any later version.
# 
# This program is distributed in the hopee that it will be useful,
# but WITHOUT ANY WARRANTTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public Licence for more details.
# 
# You should have received a copy of tthe GNU General Public Licence
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# 
# 


from LGSM.utils import Engine


class UrbanTerrorEngine(Engine):


    proto           = "udp"
    engine_number   = None # Auto registered
    port            = None # Auto registered
    config          = None # Auto registered
    context         = None # Auto registered


    def _configure(self, lxml_config):
        config = lxml_config.get_config("UrbanTerror")
        folders = lxml_config.get_folders("UrbanTerror")

        if self.config["avaibleports"]:
            self.port_range = self.config["avaibleports"]
        else:
            self.port_range = xrange(self.config["defaultport"], self.config["defaultport"] + self.config["maxinstance"])

        mode_file = self.folders["binary"] + os.sep + self.config["defaultmode"] + ".cfg"
        if not os.exists(mode_file):
            raise ConfigurationError("Unable to find the default mode file definition %s" % mode_file)

    
    def _start_arguments(self):
        self.port = get_avaible_port(self.context, portrange=self.port_range, default=self.port)

        return "+set dedicated 2 +set net_port %s +exec server.cfg +exec %s.cfg" % (
                self.port,
                self.config["defaultmode"]
                )


