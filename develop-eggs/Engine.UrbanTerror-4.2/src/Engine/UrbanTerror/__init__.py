
import os

from LGSM.lgec import LGECReader
from Engine.UrbanTerror.engine import UrbanTerrorEngine

def initialize(context):
    this_path = os.path.dirname(os.path.realpath(__file__))
    LGECReader(from_file = this_path + "/configure.lgec").register(context)


