<?xml version="1.0" encoding="utf-8" ?>
<playlist>
	<gameinfos>
		<game_mode>0</game_mode>
		<chat_time>10000</chat_time>
		<rounds_pointslimit>3</rounds_pointslimit>
		<rounds_usenewrules>1</rounds_usenewrules>
		<timeattack_limit>10000</timeattack_limit>
		<timeattack_synchstartperiod>0</timeattack_synchstartperiod>
		<team_pointslimit>5</team_pointslimit>
		<team_maxpoints>6</team_maxpoints>
		<team_usenewrules>0</team_usenewrules>
		<laps_nblaps>100</laps_nblaps>
		<laps_timelimit>0</laps_timelimit>
	</gameinfos>

	<hotseat>
		<game_mode>0</game_mode>
		<timeattack_limit>3</timeattack_limit>
		<rounds_count>5</rounds_count>
	</hotseat>

	<filter>
		<is_solo>0</is_solo>
		<is_hotseat>0</is_hotseat>
		<is_lan>1</is_lan>
		<is_internet>0</is_internet>
		<sort_index>500</sort_index>
		<random_map_order>1</random_map_order>
	</filter>

	<challenge>
		<file>Challenges\Nadeo\Nations\Pro\Pro A-0.Challenge.Gbx</file>
		<ident>zVV6pl2qVGVcPWOcOeODmt3w8n9</ident>
	</challenge>
	<challenge>
		<file>Challenges\Nadeo\Nations\Pro\Pro A-1.Challenge.Gbx</file>
		<ident>XqV_cwrwkWbu65UChCSm2rtiAOb</ident>
	</challenge>
	<challenge>
		<file>Challenges\Nadeo\Nations\Pro\Pro A-2.Challenge.Gbx</file>
		<ident>YaiKlMGPtLIuHEMUKKwV84TrvZ8</ident>
	</challenge>
	<challenge>
		<file>Challenges\Nadeo\Nations\Pro\Pro A-3.Challenge.Gbx</file>
		<ident>rTZZLVNP9qc2oRXKvhksyhCEnHf</ident>
	</challenge>
	<challenge>
		<file>Challenges\Nadeo\Nations\Pro\Pro A-4.Challenge.Gbx</file>
		<ident>yqFd1min_29U2sIHs_vsXHjV8Kj</ident>
	</challenge>
	<challenge>
		<file>Challenges\Nadeo\Nations\Pro\Pro A-5.Challenge.Gbx</file>
		<ident>CZdq5dPOcEuWeSw5VnDRtYh6jHc</ident>
	</challenge>
	<challenge>
		<file>Challenges\Nadeo\Nations\Pro\Pro A-6.Challenge.Gbx</file>
		<ident>hsZqPu6eGptYrsnffINFdfQl7Ud</ident>
	</challenge>
	<challenge>
		<file>Challenges\Nadeo\Nations\Pro\Pro A-7.Challenge.Gbx</file>
		<ident>N6Emv7C3V8Aj0omYGnJ783Q9Fhk</ident>
	</challenge>
	<challenge>
		<file>Challenges\Nadeo\Nations\Pro\Pro A-8.Challenge.Gbx</file>
		<ident>1bGe9uEvx85nOgHhF0cidEKpi95</ident>
	</challenge>
	<challenge>
		<file>Challenges\Nadeo\Nations\Pro\Pro A-9.Challenge.Gbx</file>
		<ident>JyNNj8bpwB66TNKs595z1rKU3F5</ident>
	</challenge>
</playlist>
