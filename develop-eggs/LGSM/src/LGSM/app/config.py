#!/usr/bin/python2

## 
# @file config.py
# @author Axel Martin <axel.martin@eisti.fr>
# @brief LGSM Config Manager
# @version 0.1
# @date 2015-03-09
# 

## 
# Copyright (C) 2015 Axel Martin 
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public Licence
# as published by the Free Software Foundation; eitther version 2
# of the Licence, or (at your option) any later version.
# 
# This program is distributed in the hopee that it will be useful,
# but WITHOUT ANY WARRANTTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public Licence for more details.
# 
# You should have received a copy of tthe GNU General Public Licence
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# 
# 


import os
import sys
import ast

from ConfigParser import ConfigParser
from optparse import OptionParser

from LGSM.utils.screen import list_screens
from LGSM.core.exceptions import ConfigurationError, UnavaiblePortException


class PortManager:

    def __init__(self, ports_config):
        self.ports_config = ports_config

    def get_port_engine(self, port_range):

        ports = {}
        for key, cfg in self.ports_config.items():
            if int(cfg["default"]) in port_range:
                ports[key] = int(cfg["default"])
            else:
                ports[key] = self._on_unavaible_default(port_range, cfg)

        return ports

    def _on_unavaible_default(self, port_range, port_config):
        rule = port_config["on-unavaible"]
        if rule == "none": return 0
        if rule == "same": return port_config["default"]
        if rule == "throw":
            print "[FAILURE] Port %s is not avaible. It may be used by another engine, or it's not in the port range of LGSM" % port_config["default"]
            sys.exit(1)
        
        if rule in ["increment", "decrement"]:
            if rule == "increment":
                return min(filter(lambda x: x > port_config["default"], port_range))
            else:
                return max(filter(lambda x: x < port_config["default"], port_range))
        
        raise NotImplemented("Currently, inlist and inrange are not supported.")



class LGSMConfig:


    file = None
    __lgsm_config = {}
    __user_config = None
    __config = {}
    __engine_config = {}
    __undefined = {}
    __lgsm_configuration = {}


    def __init__(self, file=None):
        self.file = file
        
        self._load_configuration()
        self._validate_configuration()
        self._generate_final_configuration()


    def update(self, values, raise_on_error=True):
        """ Update the configuration from a dictionnary """
        for section, keys in values.items():
            for key, value in keys.items():
                self._update_value(section, key, value, raise_on_error)


    @property
    def directorys(self):
        out = {}
        for key, value in self.__lgsm_configuration.items():
            if key.endswith("-directory"):
                out[key.split("-")[0]] = value
        return out


    @property
    def port_range(self):
        out = []
        for port_range in self.__lgsm_configuration["port-range"].split():
            if len(port_range.split("-")) == 1:
                out += [int(port_range)]
            else:
                start, stop = port_range.split("-")
                out += xrange(int(start), int(stop))
        return out


    @property
    def avaible_engines(self):
        return self.__engine_config.keys()


    @property
    def lgsm_config(self):
        return self.__lgsm_configuration.copy()


    def get_engine_configuration(self, engine_key, args=None, raise_on_key_error=True, from_config_file=None):
        if not engine_key in self.__engine_config.keys() and raise_on_key_error:
            raise KeyError("Engine key %s not found" % engine_key)
        elif not engine_key in self.__engine_config.keys():
            return {}

        config = self.__engine_config[engine_key].copy()
        config["configkeys"] = self.__config[engine_key].copy()

        # Update from command line
        if not args is None:
            parser = OptionParser()
            for key, value in config["configkeys"].items():
                parser.add_option("--%s" % key, default = value)
            opts, args = parser.parse_args(args)

            config["configkeys"].update(vars(opts))

        # Update from config file
        if not from_config_file is None:
            cfg = ConfigParser()
            cfg.read(from_config_file)

            config_keys = config["configkeys"].copy()
            config_keys.update(dict(cfg.items("config")))

            config.update(dict(cfg.items("engine")))
            config["config_file"] = from_config_file #FIXME destroy that shit
            config["ports"] = ast.literal_eval(config["ports"]) #FIXME that suxx

        return config

    
    def get_engine_key_from_port(self, port):
        screens = list_screens()
        for s in screens:
            n = s.name
            if not n.startswith("LGSM"):
                continue
            lgsm, engine_key, engine_port = n.split("-")

            if engine_port == port:
                return engine_key

        return None


    def get_engine_config_file_from_port(self, port):
        name = filter(lambda x: x.name.endswith(port), list_screens())[0].name
        return os.sep.join([self.directorys["run"], name[5:] + ".cfg"])


    def get_engine_config_files_from_engine_key(self, engine_key):
        name = filter(lambda x: x.name.startswith("LGSM-%s" % engine_key), list_screens())
        return map(
                lambda x=None: os.sep.join([self.directorys["run"], x.name[5:] + ".cfg"]),
                name
                )


    def find_engine_ports(self, engine_config):
        port_range = self.port_range
        run_dir    = self.directorys["run"]

        # Suppress currently used ports from the port range
        for cfg_file in os.listdir(run_dir):
            engine_key = cfg_file.split(".")[0].split("-")[-2]

            cfg = ConfigParser()
            cfg.read(os.sep.join([run_dir, cfg_file]))
            ports = ast.literal_eval(cfg.get("engine", "ports"))

            port_range = filter(lambda x: not x in map(int, ports.values()), port_range)

        manager = PortManager(engine_config["portsconfig"])
        return manager.get_port_engine(port_range)


    def generate_engine_config(self, engine_config):
        engine_config = engine_config.copy()
        config_keys = engine_config.pop("configkeys")
        cfg_file    = engine_config["config_file"]

        cfg = ConfigParser()
        cfg.add_section("engine")
        cfg.add_section("config")
        for key, value in engine_config.items():
            cfg.set("engine", key, value)

        for key, value in config_keys.items():
            cfg.set("config", key, value)

        with open(cfg_file, "w") as f:
            cfg.write(f)


    # Internal definitions
    #######################################
    def _validate_value(self, validator, v):
        return True # TODO


    def _load_configuration(self):
        """ Load the configuration of LGSM from a file """
        lgsm_cfg = ConfigParser()
        lgsm_cfg.read(self.file)

        self.__user_config = ConfigParser()

        for s in lgsm_cfg.sections():
            if s != "lgsm":
                glb_cfg = ConfigParser()
                glb_cfg.read(lgsm_cfg.get(s, "lgsmside"))
                self.__user_config.read(lgsm_cfg.get(s, "userside"))
                self.__lgsm_config[s] = glb_cfg
            else:
                self.__lgsm_configuration = {}
                for key, value in lgsm_cfg.items(s):
                    self.__lgsm_configuration[key] = value

    
    def _validate_configuration(self):
        """ Validate file configuration """
        for s in self.__user_config.sections():
            glb_cfg = self.__lgsm_config[s]
            for k, v in self.__user_config.items(s):
                if not glb_cfg.has_section("%s.%s" % (s, k)):
                    raise ConfigurationError(
                            "Unknown configuration key %s in the %s.cfg configuration file." % (k, s)
                            )
                if not glb_cfg.getboolean("%s.%s" % (s, k), "can-be-empty") and v == "" or v == None:
                    raise ConfigurationError(
                            "Configuration key %s in the %s.cfg configuration can not be empty." % (k, s)
                            )
                validator = glb_cfg.get("%s.%s" % (s, k), "validator")
                if not self._validate_value(validator, v):
                    raise ConfigurationError(
                            "Unvalid value '%s' in the %s.cfg configuration file. Expected : %s." % (v, s, validator)
                            )
    

    def _generate_final_configuration(self):
        for s in self.__user_config.sections():
            glb_cfg = self.__lgsm_config[s]
            self.__config[s] = {}
            self.__undefined[s] = []
            
            # Getting configuration keys from user file and lgsm file
            for glb_key in filter(None, glb_cfg.get(s, "configkeys").split()):
                key = glb_key.split(".")[1]
                if self.__user_config.has_option(s, key):
                    self._update_value(s, key, self.__user_config.get(s, key))
                elif glb_cfg.getboolean(glb_key, "required"):
                    self.__undefined[s].append(key)
                else:
                    self._update_value(s, key, glb_cfg.get(glb_key, "default"))

            # Getting configfile configuration
            cfgfiles = {}
            for glb_key in filter(None, glb_cfg.get(s, "configfiles").split()):
                key = glb_key.split(".")[-1]
                cfgfiles[key] = dict(glb_cfg.items(glb_key))

            # Getting port configuration
            portcfg = {}
            for glb_key in filter(None, glb_cfg.get(s, "portsconfig").split()):
                key = glb_key.split(".")[-1]
                portcfg[key] = dict(glb_cfg.items(glb_key))

            # Getting engine configuration
            self.__engine_config[s] = {}
            for key, value in glb_cfg.items(s):
                self.__engine_config[s][key] = value

            # Registering port configuration and configfile configuration
            self.__engine_config[s]["portsconfig"] = portcfg
            self.__engine_config[s]["configfiles"] = cfgfiles


    def _update_value(self, section, key, value, raise_on_error=True):
        def error(message):
            if raise_on_error:
                raise ConfigurationError(message)
            else:
                return False

        glb_cfg = self.__lgsm_config[section]
        glb_key = "%s.%s" % (section, key)
        if not glb_cfg.has_section(glb_key):
            return error("Unknown configuration key %s of %s." % (key, section))

        validator, can_empty = glb_cfg.get(glb_key, "validator"), glb_cfg.getboolean(glb_key, "can-be-empty")
        if not self._validate_value(validator, value):
            return error("Unvalid value %s for key %s of section %s. Expected: %s" % (value, key, section, validator))

        if (value == "" or value == 0 or value is None) and not can_empty:
            return error("Configuration key %s in section %s can not be empty." % (key, section))

        self.__config[section][key] = value
        if key in self.__undefined[section]:
            self.__undefined[section].pop(self.__undefined[section].index(key))

        return True


