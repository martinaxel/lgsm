#!/usr/bin/python2

## 
# @file ctl.py
# @author Axel Martin <axel.martin@eisti.fr>
# @brief Main LGSM controller
# @version 0.1
# @date 2015-03-06
# 


from optparse   import OptionParser
import sys
import os
import logging

from LGSM.utils import solve_entry_point
from LGSM.utils.screen import list_screens
from LGSM.app.config     import LGSMConfig


LGSM_USAGE = "bin/lgsm [lgsm options] {%(actions)s} [action options]"
LGSM_DESCR = """\
Lan Game Server Manager is a project that manage server engines.
"""
LGSM_COPYR = """\
Lan Game Server Manager (LGSM) (cc) AIR-EISTI 2015
"""
LGSM_EPILOG = """\
If you want additional informations about the actions, try `bin/lgsm {action} --help`
"""


class LGSM:


    config = None
    no_start = False


    def __init__(self, args = None):
        parser = OptionParser(
                usage = LGSM_USAGE % dict(actions = "|".join(self.actions.keys())),
                description = LGSM_DESCR,
                epilog = LGSM_EPILOG)

        parser.add_option("-p", "--port", dest="port", help="specify a port", type="int", metavar="PORT")
        parser.add_option("-C", "--config", dest="config", help="LGSM configuration file", metavar="CONFIG_FILE")
        parser.add_option("--show", dest="show", help="Only show the startline. Don't start.", default=False, action="store_true")
        parser.allow_interspersed_args = False
        
        opts, args = parser.parse_args(args)

        if not opts.config:
            parser.error("Option -C required")

        self.config = LGSMConfig(opts.config)

        if opts.port:
            self.config.requested_port = opts.port

        if args == []:
            parser.error("Nothing to do")
        action = args.pop(0)

        self.no_start = opts.show

        if not action in self.actions.keys():
            parser.error("Unknown action %s" % action)

        self.clear_unused_config_files()

        self.actions[action](self, args)


    def clear_unused_config_files(self):
        screens = map(lambda x: x.name[5:], list_screens())
        run_dir = self.config.directorys["run"]
        
        for file in os.listdir(run_dir):
            if file.split(".")[0] not in screens:
                print "[Warning] Configuration file %s is not used. Maybe the server has crashed." % file
                os.remove(run_dir + os.sep + file)



    def act_start(self, args):
        if args == ["--help"] or args == ["-h"] or args == []:
            print "bin/lgsm [-p PORT] start {engine-key} [engine-options]"
            print
            print "Start a server engine."
            print "If you specify the option `-p`, LGSM will try to start the engine with on the"
            print "specified port. If the specified port is already used it will return an error."
            print "If the option isn't specified, LGSM will try to start the engine on the"
            print "default(s) port(s) required by the engine configuration. If the port(s) is (are)"
            print "already in use, it will try to start with other ports if the engine"
            print "allows to do that."
            print "The installed engines keys could be retrieved by the command `bin/lgsm list`."
            return 0
        
        engine_key = args.pop(0)

        try:
            engine_config = self.config.get_engine_configuration(engine_key, args)
        except KeyError:
            print "Engine %s not found" % engine_key
            return 1

        engine_config["ports"]          = self.config.find_engine_ports(engine_config)
        engine_config["config_file"]    = os.sep.join([self.config.directorys["run"], "%s-%s.cfg" % (engine_key, engine_config["ports"]["server"])])
        
        engine = solve_entry_point(engine_config["entry_point"])(self.config, engine_key, engine_config)
        engine.start(show = self.no_start)

        print "Status of engine %s : %s" % (str(engine), engine.status)

        self.config.generate_engine_config(engine_config)


    def act_stop(self, args):
        if args == ["--help"] or args == ["-h"] or args == []:
            print "bin/lgsm stop {engine-key | port | --all}"
            print
            print "Stop a server engine."
            print "If a port is given, LGSM will try to stop the engine identified by this port."
            print "If an engine key is given, lgsm will stop ALL running engines identified by"
            print "this key."
            print "If `--all` specified, all engines will be stop."
            return 0

        argument = args.pop(0)
        if argument.isdigit():
            engine_key = self.config.get_engine_key_from_port(argument)
            cfg_files  = [self.config.get_engine_config_file_from_port(argument)]
        else:
            engine_key = argument
            cfg_files  = self.config.get_engine_config_files_from_engine_key(engine_key)

        if cfg_files == []:
            print "No engines found"
            return 1

        for cfg_file in cfg_files:
            engine_config = self.config.get_engine_configuration(engine_key, from_config_file=cfg_file)
            engine = solve_entry_point(engine_config["entry_point"])(self.config, engine_key, engine_config, load=True)
            engine.stop()
            os.remove(cfg_file)

        return 0


    def act_list(self, args):
        if args == ["--help"] or args == ["-h"]:
            print "bin/lgsm list [engine-key]"
            print
            print "List the avaible engines and the running engines."
            print "If `engine-key` is specified, LGSM will list all the running engines infos"
            print "identified by this key."
            return 0

        print "Avaible engines :"
        for e in self.config.avaible_engines:
            print " - %s" % e

        print

        screens = list_screens()
        if screens == []:
            print "No engines running."
        else:
            print "Running engines :"
            for s in list_screens():
                lgsm, engine_name, port = s.name.split("-")
                print " - [%s] on port %s" % (engine_name, port)


    def act_console(self, args):
        if args == ["--help"] or args == ["-h"]:
            print "bin/lgsm console {engine-key | port}"
            print
            print "Recover the console of an engine (if supported by the engine)."
            print "You should use `port` instead of `engine-key`. If multiple engines are"
            print "running with the key `engine-key`, LGSM will return an error."
            return 0

        argument = args.pop(0)
        if argument.isdigit():
            engine_key = self.config.get_engine_key_from_port(argument)
            cfg_file  = self.config.get_engine_config_file_from_port(argument)
        else:
            print "You can use this options only by giving a port as argument"

        if cfg_file == None:
            print "No engines found"
            return 1

        print self.config.lgsm_config
        if not self.config.lgsm_config.get("disable-warning-console", False):
            print "WARNING : you will enter in a screen console. To quit, use the keys <ctrl + A + D>."
            print "You can disable this message by setting 'disable-warning-console=True' in the buildout.cfg"
            print "Press ENTER to continue..."
            raw_input()

        engine_config = self.config.get_engine_configuration(engine_key, from_config_file=cfg_file)
        engine = solve_entry_point(engine_config["entry_point"])(self.config, engine_key, engine_config)
        engine.console()

        return 0



    def act_config(self, args):
        if args == ["--help"] or args == ["-h"]:
            print "bin/lgsm config {engine-key | port} {engine-config [engine-config [...]]}"
            print
            print "Manage the engine configuration"
            print "If `engine-key` is specified, this command will modify the engine configuration"
            print "file config/`engine-key`.cfg."
            print "If `port` is specified, this command will modify the configuration of a running"
            print "engine if this engine supports the on-fly configuration."
            return 0

        print "Currently not implemented"


    actions = {
            "start": act_start,
            "stop": act_stop,
            "list": act_list,
            "console": act_console,
            "config": act_config
            }


def main(args = None):
    LGSM(args)
