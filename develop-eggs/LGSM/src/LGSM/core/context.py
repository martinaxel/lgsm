#!/usr/bin/python
# -*- coding:utf-8 -*-

## 
# @file context.py
# @author Axel Martin <axel.martin@eisti.fr>
# @brief Definition of LGSM Context class
# @version 0.1
# @date 2015-02-11
# 

## 
# Copyright (C) 2015 Axel Martin 
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public Licence
# as published by the Free Software Foundation; eitther version 2
# of the Licence, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public Licence for more details.
# 
# You should have received a copy of the GNU General Public Licence
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# 
# 


import os


from LGSM.core.exceptions   import DuplicatedKey
from LGSM.core.exceptions   import LGECKeyError
from LGSM.lgec.lgec         import LGECDTD


class Context(object):


    lgec_registered = {} # engine_key  : LGECReader
    engines         = {} # engine_key  : GameEngine
    running_engines = {} # port        : GameEngine (loaded)
    files           = {} # engine_key  : list of files
    pre_installed   = []
    pre_installed_files = []

    buildout_directory = None


    def __init__(self, buildout, name, options):
        self.buildout_directory = buildout['buildout']['directory']
        self._dirs              = buildout['directorys']
        self._port_range        = buildout['lgsm']['port-range']
        self._lgec_dtd          = LGECDTD(buildout['lgsm'].get('lgec_dtd', 'config/lgec.dtd'))

        for d in self._dirs:
            if not os.path.exists(d):
                os.makedirs(d)


    def get_config_directory(self):
        return self._dirs["config"]


    def get_var_directory(self):
        return self._dirs["var"]


    def get_bin_directory(self):
        return self._dirs["bin"]


    def get_log_directory(self):
        return self._dirs["log"]

    
    def get_run_directory(self):
        return self._dirs["run"]
    

    def get_lgsm_config_file_path(self):
        return os.sep.join([self._dirs["var"], "lgsm.cfg"])


    def get_lgec_dtd(self):
        return self._lgec_dtd


    def get_port_range(self):
        out = []
        for port_range in self._port_range.split():
            if len(port_range.split("-")) == 1:
                out += [int(port_range)]
            else:
                start, stop = port_range.split("-")
                out += xrange(int(start), int(stop))
        return out


    def get_formated_port_range(self):
        return self._port_range


    def register_lgec(self, lgec_reader):
        lgec_key = lgec_reader.engine_key
        if lgec_key in self.lgec_registered.keys():
            raise LGECKeyError(
                    "Key '%s' used by LGECInstance %s is already used.",
                    (lgec_key, str(lgec_reader))
                    )

        self.lgec_registered[lgec_key] = lgec_reader


    def register_engine(self, engine_init, files):
        self.engines[engine_init.engine_key] = engine_init.engine
        self.files[engine_init.engine_key] = files
        #self._engines[engine_init.engine_name].pre_configure(self._lgec_registered) # FIXME: is it usefull ?


