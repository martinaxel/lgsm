#!/usr/bin/python2

## 
# @file templates.py
# @author Axel Martin <axel.martin@eisti.fr>
# @brief Templates config generation
# @version 0.1
# @date 2015-03-17
# 

## 
# Copyright (C) 2015 Axel Martin 
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public Licence
# as published by the Free Software Foundation; eitther version 2
# of the Licence, or (at your option) any later version.
# 
# This program is distributed in the hopee that it will be useful,
# but WITHOUT ANY WARRANTTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public Licence for more details.
# 
# You should have received a copy of tthe GNU General Public Licence
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# 
# 



def generate_template_config(template, dest, **kwargs):
    with open(template, "r") as f:
        text = f.read()

    text = text % kwargs

    with open(dest, "w") as f:
        f.write(text)

    return text


