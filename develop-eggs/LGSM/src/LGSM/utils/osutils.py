#!/usr/bin/python2
# -*- coding:utf-8 -*-

## 
# @file osutils.py
# @author Axel Martin <axel.martin@eisti.fr>
# @brief OS specific fonctionality
# @version 0.1
# @date 2015-02-12
# 

## 
# Copyright (C) 2015 Axel Martin 
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public Licence
# as published by the Free Software Foundation; eitther version 2
# of the Licence, or (at your option) any later version.
# 
# This program is distributed in the hopee that it will be useful,
# but WITHOUT ANY WARRANTTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public Licence for more details.
# 
# You should have received a copy of tthe GNU General Public Licence
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# 
# 


from shutil import copyfile, rmtree
import os


def copy_file(src, dst, overwrite=True, symlink=False, create_dirs=True):
    if not os.path.exists(os.path.dirname(dst)):
        if create_dirs:
            os.makedirs(os.path.dirname(dst))
        else:
            raise IOError("Destination folder does not exists : %s" % dst)

    if os.path.exists(dst) and not overwrite:
        raise IOError("File already exists : %s" % dst)

    if os.path.islink(src):
        src = os.readlink(src)

    if symlink:
        os.symlink(src, dst)
    else:
        copyfile(src, dst)


def delete_file(src):
    if not os.path.exists(src):
        raise IOError("File doesn't exists : %s" % src)

    os.remove(src)


def delete_directory(src, recursive=False):
    if recursive:
        os.rmtree(src)
    else:
        os.removedirs(src)


def get_system_info():
    print "Returning dummy system infos"
    return "Linux", "64bits"
