#!/usr/bin/python
# -*- coding:utf-8 -*-

## 
# @file engine.py
# @author Axel Martin <axel.martin@eisti.fr>
# @brief LGSM Basic engine implementation.
# @version 0.1
# @date 2015-02-11
# 
# All your engines definition should be inherited from the object GameEngine,
# except if you really know what you are doing. When I'm writing this, I also
# don't really know if it will be the final version of the basic engine. Then,
# just implement the abstracts methods !


import os
import pdb # FIXME

from LGSM.utils.screen import Screen, list_screens
from LGSM.utils.template import generate_template_config


class Engine(object):

    
    lgsm_config     = None
    engine_config   = None
    screen          = None


    def __init__(self, lgsm_config, name, engine_config, load=True):
        self.lgsm_config    = lgsm_config
        self.engine_config  = engine_config
        self.config_keys    = engine_config["configkeys"]
        self.key            = name

        self.ports          = engine_config["ports"]
        self.config_file    = engine_config["config_file"]
        
        self.screen_name = "LGSM-%s-%s" % (self.key, self.ports["server"]) # FIXME

        if load:
            self.screen = Screen(self.screen_name)


    def __str__(self):
        return "%s [%s] (%s)" % (self.engine_config["name"], self.key, self.ports["server"]) # FIXME

    
    @property
    def status(self):
        if self.screen_name in map(lambda x: x.name, list_screens()):
            return "running"
        return "not running"


    # Engine start related functions definition
    #######################################
    def start(self, show=False):
        if not show:
            self.pre_start_config()

        self.generate_config_files()

        self._start(show)

        if not show:
            self.post_start_config()


    def _start(self, show=False):
        binary = self.engine_config["binary"]
        if not show:
            self.screen = Screen(
                    self.screen_name,
                    start_command = " ".join([binary] + self.start_arguments())
                    )
        else:
            print "Start command : ", " ".join([binary] + self.start_arguments())


    def pre_start_config(self):
        """ This should be inherited in your engine implementation """
        pass


    def generate_config_files(self):
        root = self.engine_config["root"] + os.sep
        config = self.engine_config["configkeys"].copy()
        config.update(dict([("port." + k, v) for k, v in self.ports.items()]))

        for cfg_key in self.engine_config["configfiles"].keys():
            cfg_file = self.engine_config["configfiles"][cfg_key]

            if cfg_file["on-generate"] == "overwrite":
                if os.path.exists(root + cfg_file["dest"]):
                    os.remove(root + cfg_file["dest"])
            elif cfg_file["on-generate"] == "auto-generate":
                cfg_file["dest"] = "%s.%s.%s" % (
                        ".".join(cfg_file["dest"].split(".")[:-1]),
                        self.ports["server"],
                        cfg_file["dest"].split(".")[-1]
                        )

            generate_template_config(
                    root + cfg_file["path"],
                    root + cfg_file["dest"],
                    **config
                    )
            self.engine_config["configfiles"][cfg_key] = cfg_file["dest"]
    

    def start_arguments(self):
        """ This should be inherited in your engine implementation """
        return []


    def post_start_config(self):
        """ This should be inherited in your engine implementation """
        pass



    # Engine stop related function definition
    #######################################
    def stop(self):
        self.pre_stop_config()

        self._stop()

        self.post_stop_config()

    
    def _stop(self):
        self.screen.kill()


    def pre_stop_config(self):
        pass

    
    def post_stop_config(self):
        pass
    

    # Engine console load
    #######################################
    def console(self):
        self.screen.recover()


    # Some usefull methods for preconfigs
    #######################################
    def change_to_binary_directory(self):
        os.chdir(os.sep.join([self.lgsm_config.directorys["bin"], self.key]))
