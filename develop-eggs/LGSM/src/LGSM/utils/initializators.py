#!/usr/bin/python
# -*- coding:utf-8 -*-

## 
# @file initializators.py
# @author Axel Martin <axel.martin@eisti.fr>
# @brief LGSM Utils for plugin initialisation
# @version 0.1
# @date 2015-02-11
# 

## 
# Copyright (C) 2015 Axel Martin 
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public Licence
# as published by the Free Software Foundation; eitther version 2
# of the Licence, or (at your option) any later version.
# 
# This program is distributed in the hopee that it will be useful,
# but WITHOUT ANY WARRANTTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public Licence for more details.
# 
# You should have received a copy of tthe GNU General Public Licence
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# 
# 


import os
import stat

from LGSM.utils.osutils import delete_file, copy_file, get_system_info


class EngineInstallError(Exception):


    def __init__(self, engine_name, string):
        self.string = "Unable to start engine '%s' : %s" % (engine_name, string)


    def __str__(self):
        return self.string



class EngineRegistrationError(Exception):

    
    def __init__(self, engine_name, string):
        self.string = "Unable to register engine '%s' : %s" % (engine_name, string)


    def __str__(self):
        return self.string



class EngineInit(object):
    """ GameEngineInit : Game engine initializator
    Game Engine manager that allow you to install or remove Game Engines
    """


    __registered        = False
    __lgec              = None
    __options           = None


    def __init__(self, lgec, options={}):
        self.__lgec             = lgec
        self.__options          = options

    
    @property
    def engine(self):
        return self.__lgec.engine


    @property
    def engine_name(self):
        return self.__lgec.engine_name


    @property
    def engine_key(self):
        return self.__lgec.engine_key
    
    
    @property
    def package(self):
        return self.__lgec.master_package


    @property
    def is_registered(self):
        return self.__registered


    def install(self, context, update=False):
        """ Main installer controller

        @param context      LGSM Context
        @param update       is this already installed ?
        """
        self.context = context

        # Source and destination directory. Destination directory
        src_dir = os.sep.join([
                    os.path.dirname(os.path.realpath(self.package.__file__)),
                    self.__lgec.folders["base"]
                ])
        dst_dir = os.sep.join([
                    self.context.buildout_directory,
                    'bin',
                    self.engine_key
                ])

        # Building install tree
        tree = self._build_tree(src_dir)

        # Install engine binarys
        self.__installed_files = []
        try:
            self._install(src_dir, dst_dir, tree)
        except EngineInstallError:
            for file in self.__installed_files:
                delete_file(file)
            raise

        # Register the engine
        self.context.register_engine(self, [dst_dir])


    def _build_tree(self, base_directory):
        """ Copy tree constructor

        @param base_directory   engine deploy directory
        @return dictionnary     referencing each files and their options
        """
        
        def apply_rule(base_path, match_path, rule):
            keys = filter(lambda x: x.startswith(match_path), base_path.keys())
            for k in keys:
                base_path[k].update(rule)
            return base_path

        # Building basic tree
        paths = {} # Path : (type, options)
        default = {"os": "all", "arch": "all", "type": self.__lgec.folders["default"].lower()}
        cutter = len(base_directory) + 1

        for root, dirs, files in os.walk(base_directory):
            for f in files:
                paths[os.sep.join([root, f])[cutter:]] = default.copy()

        # Applying xml rules
        for s in self.__lgec.folders["specific"]:
            s["type"] = s["type"].lower()
            path = s["path"]
            paths = apply_rule(paths, path, s)

        return paths

    
    def _install(self, src_dir, dst_dir, tree):
        """ Sub install process

        @param src_dir          file source directory
        @param dst_dir          file destination directory
        @param tree             dictionnary referencing each files and their options
        """
        binary_path = None
        this_os, this_arch = get_system_info()

        for file, config in tree.items():
            src_file, dst_file = os.sep.join([src_dir, file]), os.sep.join([dst_dir, file])
            if_exists = config.get("if-exists", "overwrite")

            # Checking if file should be copied or not
            file_os, file_arch = config["os"], config["arch"]
            if (config["type"] == "nocopy") ^ (not file_os in [this_os, "all"] and not file_arch in [this_arch, "all"]):
                continue
            if os.path.exists(dst_file):
                if if_exists == "nothing":
                    continue
                elif if_exists == "overwrite":
                    os.remove(dst_file)
                elif if_exists == "ask":
                    answer = raw_input("The file %s already exists. Should I remove it ? [y/N] : ")
                    if answer.lower() in ["y", "yes"]:
                        os.remove(dst_file)
                    else:
                        continue
                elif if_exists == "failure":
                    raise EngineInstallError(self.engine_name, "file %s exists, and the configuration of the engine asks to fail on existance." % dst_file)

            # Actually copy the file :)
            if config["type"] == "hard":
                copy_file(src_file, dst_file, symlink=False)
            elif config["type"] == "binary":
                copy_file(src_file, dst_file, symlink=config["symlink"] == "true")
                if this_os == "Linux": # Make it executable
                    os.chmod(dst_file, os.stat(dst_file).st_mode | stat.S_IEXEC)
                binary_path = dst_file
            else:
                copy_file(src_file, dst_file, symlink=True)
            
            self.__installed_files.append(dst_file)

        # Checking if there's a binary path matching
        if binary_path == None:
            raise EngineInstallError(self.engine_name, "no binary found for this system.")
        
        self.engine.binary = binary_path


    def uninstall(self, context, hard=False):
        """ FIXME: this is not the right way to uninstall """
        root = context.root_directory

        for folder in self.__lgec_config.folder:
            if folder["static"] == "false":
                dst = os.sep.join([root, folder["type"], self.__engine_name, folder])
                if not os.path.exists(dst) and not hard:
                    raise EngineUninstallError(self.__engine_name, "folder '%s' does not exists." % dst)
                delete_folder(os.sep.join([root, folder["type"], self.__engine_name, folder]))
                context.unregister_folder(self.__engine_name, folder)

        context.unregister_package(self.engine_name)
        context.unregister_game_engine(self.engine_name)

