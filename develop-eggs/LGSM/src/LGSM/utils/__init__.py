#!/usr/bin/python
# -*- coding:utf-8 -*-

## 
# @file __init__.py
# @author Axel Martin <axel.martin@eisti.fr>
# @brief LGSM Utilitaires Import Manager
# @version 0.1
# @date 2015-02-12
# 
# This file import all utilitaires you'll need to make your own game engine

## 
# Copyright (C) 2015 Axel Martin 
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public Licence
# as published by the Free Software Foundation; eitther version 2
# of the Licence, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public Licence for more details.
# 
# You should have received a copy of the GNU General Public Licence
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# 
# 


from LGSM.utils.initializators  import EngineInit
from LGSM.utils.engine          import Engine
import importlib


def solve_entry_point(entry_point, base_package=None):
    module_name, object_name = entry_point.split(":")
    
    if module_name == "":
        package = base_package
        if package is None:
            raise ImportError("Relative import doesn't work without a base package registered.")
    else:
        package = importlib.import_module(module_name)

    if object_name != "":
        return getattr(package, object_name)
    return package
