#!/usr/bin/python2
# -*- coding:utf-8 -*-

## 
# @file config.py
# @author Axel Martin <axel.martin@eisti.fr>
# @brief LGSM Config Manager
# @version 0.1
# @date 2015-02-12
# 


class Config(object):

    
    __config = {}


    def __init__(self):
        pass


    def get(self, key):
        return self.__config.get(key, None)
    

    def set(self, key):
        if self.protected
