#!/usr/bin/python
# -*- coding:utf-8 -*-

## 
# @file lgec.py
# @author Axel Martin <axel.martin@eisti.fr>
# @brief LGSM Engine Game Configuration file (*.lgec) reader
# @version 0.1
# @date 2015-02-12
# 

## 
# Copyright (C) 2015 Axel Martin 
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public Licence
# as published by the Free Software Foundation; eitther version 2
# of the Licence, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public Licence for more details.
# 
# You should have received a copy of the GNU General Public Licence
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# 


from lxml.etree import XML, DTD, DTDError, DTDParseError
import importlib
import pdb

from LGSM.core.exceptions import ContextError, LGECKeyError


__LGEC_VERSION__ = "0.4"


class LGECDTD(DTD):
    """ DTD Validator and parser for LGEC XML Config File

    @author Axel Martin
    @version 0.1
    @date 2015-02-14
    
    This class stores the Document Type Definition of LGEC file and allow
    LGECReader to parse datas from XML. Data parsed will store as python-like
    format tag content like `Folders` or `MetaData`.

    In version 0.1 :
        `MetaData`      tag parsing : {"SubElementName" : "SubElementValue"}
        `Folders`       tag parsing : [{"ElementParamName" : "ElementParamValue"}]
        `Binary`        tag parsing : [{"ElementParamName" : "ElementParamValue"}]
        `ConfigKeys`    tag parsing : {"@key" :
                                        {"ElementParamName" : "ElementParamValue"}
                                      }
    """


    __version__ = __LGEC_VERSION__


    def __init__(self, *args):
        """ Initialisation method

        This method will just check if `__version__` entity is declared in the
        DTD file, in order to check it with parser version. See
        lxml.etree.DTD.__init__ documentation for more informations.
        """
        DTD.__init__(self, *args) # FIXME : il faudrait passer par une super
        self._check_version()


    def _check_version(self):
        """ Version checker

        This method will search the `__version__` entity in the DTD, in order
        to check the compatibility between DTD file and parser.
        """
        entities = self.entities()
        for e in entities:
            if e.name == "__version__":
                if e.content == self.__version__:
                    return
                else:
                    raise DTDError("LGECDTD version doesn't match with lgec.dtd version.")
        raise DTDParseError("DTD file doesn't have any '__version__' entity declared.")

    
    def find_element(self, element_name):
        """ Find an element declaration in the DTD file

        @param element_name str     element name to find
        @return lxml.etree._DTDElementDecl node corresponding, or None if not found.
        """
        elements = self.elements()
        for e in elements:
            if e.name == element_name:
                return e
        return None
    

    def parse_meta_data(self, xml):
        """ Parse the MetaDatas of an LGEC file. """
        dtd_metadata = self.find_element("MetaData")
        xml_metadata = xml.find("MetaData")

        tag = dtd_metadata.content
        datas = {}
        while tag.type == "seq":
            datas[tag.left.name] = self.__find_content(xml_metadata, tag.left.name)
            tag = tag.right
        datas[tag.name] = self.__find_content(xml, tag.name)
        
        return datas

    
    def parse_port_config(self, xml):
        """ Parse the port configuration of an LGEC file. """
        dtd_port = self.find_element("Port")
        xml_port = xml.findall("Port")

        port = map(lambda x: self.__find_attribs(x, dtd_port), xml_port)
        output = {}
        for p in port:
            key = p.pop("key")
            if key in output.keys():
                raise LGECKeyError("Duplicated key '%s' in port configuration." % key)
            output[key] = p

        return output


    def parse_folders(self, xml):
        """ Parse the Folder tags of an LGEC file. """
        dtd_folders = self.find_element("FolderConfig")
        xml_folders = xml.findall("FolderConfig")

        base = xml_folders[0].attrib['base']

        return dict(
                base=base,
                default=self.__find_attribs(xml_folders[0], dtd_folders)["default"],
                specific=map(
                    lambda x: dict(
                        type = x.tag,
                        **self.__find_attribs(x, self.find_element(x.tag))
                    ),
                    xml_folders[0].getchildren()
                )
            )


    def parse_binary(self, xml):
        """ Parse the Binary tags of an LGEC file. """
        dtd_binary = self.find_element("Binary")
        xml_binary = xml.findall("Binary")
        
        return map(lambda x: self.__find_attribs(x, dtd_binary), xml_binary)


    def parse_config_keys(self, xml):
        """ Parse the ConfigKey tags of an LGEC file. """
        dtd_configkeys = self.find_element("ConfigKey") 
        xml_configkeys = xml.findall("ConfigKey")

        config = map(lambda x: self.__find_attribs(x, dtd_configkeys), xml_configkeys)
        datas  = {}
        for c in config:
            datas[c.pop("key")] = c
        return datas
    

    def __find_attribs(self, xml_element, dtd_element, default=True):
        datas = {}
        for e in dtd_element.attributes():
            datas[e.name] = e.default_value
        datas.update(xml_element.attrib)
        return datas


    def __find_content(self, xml, tag_name):
        d = xml.find(tag_name)
        if d != None:
            return d.text
        else:
            return None



class LGECReader(object):
    """ LGSM Game Engine Configuration reader

    @author Axel Martin
    @version 0.2
    @date 2015-02-13
    
    This class stores a LGSM configuration for a specific game engine.
    This configuration can be load from a file (from_file) XOR from a XML
    string. 
    All the LGEC XML have to be validated by the Document Type Definition
    defined in the LGSM engine (LGSM.egg/LGSM/static/lgec.dtd)
    """

    __lgec          = None
    __xml_node      = None
    __registered    = False
    __package       = None
    __engine_init   = None
    __engine        = None

    __metadata      = {}
    __port_config   = {}
    __folders       = []
    __binarys       = []
    __configkeys    = {}


    def __init__(self, from_file = None, text_lgec = None):
        """ Initialisation method

        @param from_file str    read the xml from a file
        @param text_lgec str    read the xml from a string
        @note this two parameters can't be used together
        """
        if from_file != None and text_lgec != None:
            raise NotImplementedError("Initialisation of LXMLReader from file and string is not implemented")
        if from_file:
            try:
                f = open(from_file, "r")
            except:
                raise IOError("File '%s' not found." % from_file)
            text_lgec = f.read()

        self.__lgec = XML(text_lgec)


    def __str__(self):
        if self.__registered:
            return "<LGECReader Instance [UNREGISTERED]>"
        else:
            return "<LGECReader Instance key='%s' package='%s'>" % (
                    self.master_key, self.master_package
                    )


    def register(self, context):
        """ Validity checkup and registration of the LGSM Game Engine Configuration file

        @param context Context  registration handler
        """
        dtd = context.get_lgec_dtd()
        if not dtd.validate(self.__lgec):
            raise DTDError("Unvalid LGSM Game Engine Configuration XML.")

        # Importing master package for the engine
        try:
            self.__package = importlib.import_module(self.__lgec.attrib["package"])
        except:
            raise ImportError("Unable to import package %s." % self.__lgec.attrib["package"])

        # Importing engine
        try:
            module_name, class_name = self.__lgec.attrib["engine"].split(":")
            if module_name == "":
                engine_module = self.__package
            else:
                engine_module = importlib.import_module(self.__lgec.attrib["engine"].split(":")[0])
            self.__engine = getattr(engine_module, self.__lgec.attrib["engine"].split(":")[1])
        except:
            raise ImportError("Unable to import engine %s." % self.__lgec.attrib["engine"])
        
        # Importing engine installer
        default_installer = filter(
                lambda x: x.name == "installer",
                dtd.find_element("Engine").attributes()
            )[0].default_value
        installer = self.__lgec.attrib.get("installer", default_installer)
        try:
            engine_init_module = importlib.import_module(installer.split(":")[0])
            self.__engine_init = getattr(engine_init_module, installer.split(":")[1])
        except:
            raise ImportError("Unable to import installer %s." % installer)

        # data parsing
        self.__metadata     = dtd.parse_meta_data(self.__lgec) # MetaData parsing
        self.__port_config  = dtd.parse_port_config(self.__lgec)
        self.__folders      = dtd.parse_folders(self.__lgec)
        self.__binarys      = dtd.parse_binary(self.__lgec)
        self.__configkeys   = dtd.parse_config_keys(self.__lgec)

        self.__registered = True
        context.register_lgec(self)


    @property
    def engine_key(self):
        return self.metadata["EngineKey"]


    @property
    def engine_name(self):
        return self.metadata["FullName"]


    @property
    def master_package(self):
        if not self.__registered:
            raise ContextError("Unable to use this function on an unregistered LGECReader")
        return self.__package


    @property
    def port_config(self):
        if not self.__registered:
            raise ContextError("Unable to use this function on an unregistered LGECReader")
        return self.__port_config.copy()


    @property
    def engine(self):
        if not self.__registered:
            raise ContextError("Unable to use this function on an unregistered LGECReader")
        return self.__engine


    @property
    def engine_entry_point(self):
        if not self.__registered:
            raise ContextError("Unable to use this function on an unregistered LGECReader")
        return self.__lgec.attrib["engine"]


    @property
    def engine_installer(self):
        if not self.__registered:
            raise ContextError("Unable to use this function on an unregistered LGECReader")
        return self.__engine_init
    

    @property
    def metadata(self):
        if not self.__registered:
            raise ContextError("Unable to use this function on an unregsitered LGECReader")
        return self.__metadata.copy()


    @property
    def folders(self):
        if not self.__registered:
            raise ContextError("Unable to use this function on an unregsitered LGECReader")
        return self.__folders.copy()


    @property
    def binarys(self):
        if not self.__registered:
            raise ContextError("Unable to use this function on an unregistered LGECReader")
        return self.__binarys[:]

    
    @property
    def configkeys(self):
        if not self.__registered:
            raise ContextError("Unable to use this function on an unregistered LGECReader")
        return self.__configkeys.copy()


