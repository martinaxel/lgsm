#!/usr/bin/python2
# -*- coding:utf-8 -*-

## 
# @file setup.py
# @author Axel Martin <axel.martin@eisti.fr>
# @brief Setup of LGSM.recipe.lgsm
# @version 0.1
# @date 2015-02-17
# 

## 
# Copyright (C) 2015 Axel Martin 
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public Licence
# as published by the Free Software Foundation; eitther version 2
# of the Licence, or (at your option) any later version.
# 
# This program is distributed in the hopee that it will be useful,
# but WITHOUT ANY WARRANTTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public Licence for more details.
# 
# You should have received a copy of tthe GNU General Public Licence
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# 
# 


from setuptools import setup, find_packages


version = "0.1"
name = "LGSM.recipe.lgsm"



setup(
    name                    = 'LGSM.recipe.lgsm',
    version                 = '0.1',
    author                  = 'Axel Martin',
    author_email            = 'axel.martin@eisti.fr',
    package_dir             = {'' : 'src'},
    packages                = find_packages('src'),
    namespace_packages      = ['LGSM', 'LGSM.recipe'],
    install_requires        = ['setuptools', 'zc.buildout', 'zc.recipe.egg', 'LGSM'],
    include_package_data    = True,
    zip_safe                = False,
    entry_points            = {
            'zc.buildout': ['default = %s:Recipe' % name]
        }
    )
