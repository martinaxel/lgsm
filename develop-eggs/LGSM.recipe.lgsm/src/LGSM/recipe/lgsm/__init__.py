#!/usr/bin/python2
# -*- coding:utf-8 -*-

## 
# @file __init__.py
# @author Axel Martin <axel.martin@eisti.fr>
# @brief LGSM recipe for creation of lgsm binary
# @version 0.2
# @date 2015-02-16
# 


import logging
import os
import sys
import pdb

from zc.recipe.egg import Egg
import zc.buildout

from LGSM.core.context import Context
from LGSM.recipe.lgsm.confgen import ConfigManager


## 
# @author Axel Martin
# @brief LGSM Conf-gen main recipe
# @version 0.2
# @date 2015-02-20
# 
# This create a configuration from the buildout configuration files.
class Recipe:
    """ LGSM Recipe for Buildout """


    def __init__(self, buildout, name, options):
        """ Initialisation

        @param buildout 
        @param name
        @param options
        """
        self.buildout = buildout
        self.name = name
        self.options = options
        self.binary = options.get("binary")

        self.logger = logging.getLogger(self.name)

        self.egg = Egg(buildout, options["recipe"], options)

        self.context = Context(buildout, "context", options)

        self.confgen = ConfigManager(self.options.get("config_folder", os.sep.join(["vars", "lgsm"])))
        

    def install(self, update=False):
        """ Main install method

        This is a specific buildout method. This will create the requested
        environments for every engines and create the final lgsm script

        @param update       is this an install or an update ?
        @return             the list of files created by this installation
        """
        requirements, ws = self.egg.working_set(['LGSM.recipe.lgsm'])

        # Create directorys if the don't exists
        for dir in self.buildout["directorys"].values():
            if not os.path.exists(dir):
                os.makedirs(dir)
        
        # Looking for entry points for LGSM
        for egg in ws:
            self._initialize_module(egg)

        # Checking if some files should be remove
        if update:
            self.confgen.get_precedent_configuration(self.context)
            self.context.update = True

        # Installation process
        self.this_install_files = []
        try:
            self._proceed_installation(update)
        except: # Cleaning
            for f in self.this_install_files:
                os.remove(f)
            raise
        
        # Generating configuration
        files = self.confgen.generate_configuration(self.context)

        # Generating scripts
        files += zc.buildout.easy_install.scripts(
            dest        = self.buildout["buildout"]["bin-directory"],
            reqs        = [(self.name, 'LGSM.app.ctl', 'main')],
            working_set = ws,
            executable  = self.options.get("executable", sys.executable),
            extra_paths = [],
            interpreter = 'python',
            arguments   = "['-C', '%s'] + sys.argv[1:]" % self.context.get_lgsm_config_file_path(),
            )

        # Deleting unused files
        for f in filter(lambda x: not x in files, self.context.pre_installed_files):
            if os.path.exists(f):
                if os.path.isdir(f):
                    delete_directory(f, recursive=True)
                else:
                    delete_file(f)

        return files
        
    
    def _initialize_module(self, egg):
        """ Engine initialisation. For every eggs, we will look if there's (a)
        `LGSM` entry point. If there's, then we will initialize every entry
        points found in the egg.

        @param egg          the egg to initialize
        """
        entry_map = egg.get_entry_map()
        if entry_map.has_key('LGSM'):
            egg.activate()
            for entry_point in entry_map['LGSM'].values():
                entry_point.load()(self.context)

    
    def _proceed_installation(self, update=False):
        """ Engines installation

        This will instantiate every engine installer and install the engines.

        @param update        is this an update or an install ?
        """
        # Installing (or updating) engines
        for key, lgec in self.context.lgec_registered.items():
            installer = lgec.engine_installer(lgec, self.buildout.get(key, {}))
            installer.install(self.context, key in self.context.pre_installed)


    def update(self):
        """ Main update method

        This is a specific buildout method. It will just call install method
        with parameter "update=True". That's it.
        """
        return self.install(True)

