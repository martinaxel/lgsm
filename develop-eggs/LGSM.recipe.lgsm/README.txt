Metadata-Version: 1.1
Name: LGSM.recipe
Version: 0.1
Summary: Create lgsm core script
Author: Axel Martin
Author-email: axel.martin@eisti.fr
License: GPL
Description: Introduction
        ============

        The ``LGSM.recipe`` will create the main LGSM executable. To use, just add in
        the buildout configuration the following lines :

            [buildout]
            parts = lgsm

            [lgsm]
            recipe = LGSM.recipe.lgsm
            binary = ${buildout:bin-directory}/lgsm

        This will create the LGSM manager python script at the `binary` path indicated.


        Reference
        =========

        The following options are supported :

        binary
            Path to final binary.


        Changes
        =======

        0.1 - 2015-02-16
        ----------------

        * Creation of the egg
Keywords: buildout lgsm recipe

