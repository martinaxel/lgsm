# TODO List pour les features

## Pré-lancement de l'application

- Sérialisation des classes engines plutôt qu'un chargement via fichier de configuration
- Changement de nom (LGSM -> HADES)

## Features

- Implémentation d'un service web permettant de gérer l'application par graphique
