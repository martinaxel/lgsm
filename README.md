# LGSM : Lan Game Start Manager


## Authors

- Axel `FunkySayu` Martin [axel.martin@eisti.fr]
- Maxime `qNk` Fourcade [maxime.fourcade@eisti.fr]

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the GNU Public License 2 or upper.
Please ask if you wish a more permissive license.


## Installation

To install LGSM, the following steps are required :

    $ git clone git@gitlab.etude.eisti.fr:martinaxel/lgsm.git   # Download the project
    $ python2 bootstrap.py                                      # Install buildout
    $ bin/buildout                                              # Deploy LGSM


## Configuration

The configuration is made in the `buildout.cfg` file. All the configuration
keys are commented.


## How to use

    bin/lgsm [lgsm parameters] {start|list|stop|console} {engine-key|engine-port} [engines parameters]

Here are the currently supported features of LGSM :

    bin/lgsm [-p PORT] start engine-key [engine parameters] -- [additional command lines arguments for the server]
        Start an engine with some parameters

    bin/lgsm list
        List all current running engines

    bin/lgsm console [engine port]
        Recover the screen of an engine

    bin/lgsm stop {engine-port|engine-key}
        Stop an (or multiple if you use a key) engine


## Typical GameEngine manager egg for LGSM2


### File composition :


    Engine.[EngineName].egg/                # Root folder
        | README.txt                        # Contain informations about the egg
        | setup.py                          # Python setup of the egg
        | src/                              # Python code source
        |   | Engine/                       # First level module. Do not place anything else here
        |   |   | __init__.py               # Common file (see http://peak.telecommunity.com/DevCenter/setuptools#namespace-packages)
        |   |   | [EngineName]/             # The engine module
        |   |   |   | configure.lgec        # XML Configuration of the egg
        |   |   |   | __init__.py           # Contains the recipe for LGSM
        |   |   |   | engine.py             # Container of all python-engine
        |   |   |   | deploy/               # [not required] if the server require specific files to be statics, use this folder (statics configs...)


